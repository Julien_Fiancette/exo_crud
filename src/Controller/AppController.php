<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Component\HttpFoundation\Request;

class AppController extends AbstractController
{
    
    public function home()
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository(Product::class)->findAll();
        return $this->render('app/home.html.twig', [
            'products' => $products
        ]);
    }

    public function product($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->findOneBy(['id' => $id]);
        return $this->render('app/product.html.twig', [
            'product' => $product
        ]);
    }

    public function addProduct(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();
            $em->persist($product);
            $em->flush();
            return $this->redirectToRoute('home');
        }

        return $this->render('app/form_product.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editProduct(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();
            $em->persist($product);
            $em->flush();
            return $this->redirectToRoute('home');
        }

        return $this->render('app/form_product.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function deleteProduct($id) {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->findOneBy(['id' => $id]);
        $em->remove($product);
        $em->flush();
        return $this->redirectToRoute('home');
    }
}
